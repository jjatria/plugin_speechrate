speechrate
==========

Description
-----------

A CPrAN-compatible version of de Jonge and Wempe's syllable nuclei
detection script.

Requirements
------------

* `selection`

