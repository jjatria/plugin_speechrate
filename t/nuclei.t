include ../../plugin_tap/procedures/more.proc

@no_plan()

silence = Create Sound from formula: "silence", 1, 0, 0.1, 44100, "0"
speechrate$ = preferencesDirectory$ + "/plugin_speechrate/scripts/"

if praatVersion >= 6036
  synth_language$ = "English (Great Britain)"
  synth_voice$ = "Male1"
  syllable_delta = 1
else
  synth_language$ = "English"
  synth_voice$ = "default"
  syllable_delta = 0
endif

synth = Create SpeechSynthesizer: synth_language$, synth_voice$
text = To Sound: "Our's not to question why, our's just to do or die", "no"

sound = Concatenate
sound$ = selected$("Sound")

removeObject: silence, text

selectObject: sound
runScript: speechrate$ + "syllable_nuclei.praat",
  ... 0, -10, 2, 0.3, 50, 0.08

@is: numberOfSelected("TextGrid"), 1,
  ... "script creates a TextGrid"

textgrid = selected("TextGrid")

@is$: selected$("TextGrid"), sound$,
  ... "created TextGrid has same name as Sound"

@is: do("Get number of tiers"), 1,
  ... "created TextGrid has one tier"

@is: do("Is interval tier...", 1), 0,
  ... "created TextGrid tier is point tier"

@is$: do$("Get tier name...", 1), "nuclei",
  ... "created TextGrid tier has correct name"

@is: do("Get number of points...", 1), 12 + syllable_delta,
  ... "script counted syllables well"

selectObject: sound
runScript: speechrate$ + "syllable_nuclei.praat",
  ... 1, -10, 2, 0.3, 50, 0.08

@is: numberOfSelected("TextGrid") + numberOfSelected("Table"), 2,
  ... "script creates a TextGrid and Table summary"

old      = textgrid
textgrid = selected("TextGrid")
summary  = selected("Table")

@is: objectsAreIdentical(old, textgrid), 1,
  ... "TextGrid does not change with summary"

removeObject: old, textgrid

selectObject: summary
list$ = List: "no"
columns$ = extractLine$(list$, "")
@is$: columns$,
  ... "soundname" + tab$ +
  ... "nsyll" + tab$ +
  ... "npause" + tab$ +
  ... "dur" + tab$ +
  ... "phonationtime" + tab$ +
  ... "speechrate" + tab$ +
  ... "articulation_rate" + tab$ +
  ... "ASD",
  ... "summary Table has correct column names"

removeObject: summary

selectObject: sound
pitch = To Pitch: 0, 75, 600

selectObject: sound, pitch
runScript: speechrate$ + "syllable_nuclei.praat",
  ... 0, -10, 2, 0.3, 50, 0.08
textgrid = selected("TextGrid")

@is: numberOfSelected("TextGrid"), 1,
  ... "script runs on Sound and Pitch combo"

removeObject: pitch, textgrid

removeObject: sound, synth

@ok_selection()

@done_testing()
