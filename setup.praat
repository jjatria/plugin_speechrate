# Setup script for speechrate
#
# This script is part of the speechrate CPrAN plugin for Praat.
# The latest version is available through CPrAN or at
# <https://gitlab.com/jjatria/plugin_speechrate>
#
# The speechrate plugin is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# The speechrate plugin is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with utils. If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2016 José Joaquín Atria

## Static commands:

## Dynamic commands:
Add action command: "Sound", 0, "",      0, "", 0, "Find syllable nuclei...", "Query -", 1, "scripts/syllable_nuclei.praat"
Add action command: "Sound", 0, "Pitch", 0, "", 0, "Find syllable nuclei...", "Query -", 1, "scripts/syllable_nuclei.praat"
